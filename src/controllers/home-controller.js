module.exports = mongoDB => {
  async function getAllData(req, res) {
    const response = await mongoDB
      .collection('alldata')
      .find({ _type: 'ReportsCommonType' })
      .toArray();

    res.render('index', { title: 'Bot Report', data: response });
  }

  return {
    getAllData
  };
};
