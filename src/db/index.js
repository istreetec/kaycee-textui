const { MongoClient } = require('mongodb');
const path = require('path');
require('dotenv').config({ path: path.join(__dirname, '../../.env') });

async function mongoDbPool() {
  // MongoDB Connection.
  let url = '';
  let user = '';
  let password = '';
  let dbName = '';

  // Switch Between Production and Dev Environments.
  const cleanEnv = process.env.ENV
    ? process.env.ENV.toLowerCase()
    : 'development';
  const isDevEnv = cleanEnv === 'development';

  if (isDevEnv) {
    url = process.env.URL;
    user = process.env.COSMODDBUSER;
    password = process.env.COSMOSDBPASSWORD;
    dbName = process.env.COSMOSDB;
  } // EnIf

  // e.g. KCB Azure Tenant.
  url = process.env.KCB_URL;
  user = process.env.KCB_MONGODBUSER;
  password = process.env.KCB_MONGODBPASS;
  dbName = process.env.KCB_MONGODB;

  // Authenticate
  const dbAuth = {
    auth: { user, password },
    useNewUrlParser: true,
    useUnifiedTopology: true
  };

  return MongoClient.connect(url, dbAuth).then(client => client.db(dbName));
}

module.exports = async () => {
  const dbPools = await Promise.all([mongoDbPool()]);

  return {
    mongoDB: dbPools[0]
  };
};
