const path = require('path');
const express = require('express');
const morgan = require('morgan');

const app = express();

app.use(morgan('tiny'));

// Static Files
app.use(express.static(path.join(__dirname, '../public')));

// Templating engine
app.engine('pug', require('pug').__express);

app.set('view engine', 'pug');
app.set('views', path.join(__dirname, './views'));

module.exports = app;
