const homeRouter = require("./home-router");
const errorHandlers = require("../helpers/error-handlers");

async function initRoutes(app, dbPool) {
  // Routes
  app.use("/", homeRouter(dbPool));

  // Error MiddleWares After our Routes.
  //
  // If that above routes didnt work, we 404 them and forward to error handler
  app.use(errorHandlers.notFound);

  // One of our error handlers will see if these errors are just validations.
  app.use(errorHandlers.flashValidationErrors);

  if (process.env.NODE_ENV === "development") {
    // Development Error Handler - Prints stack trace
    app.use(errorHandlers.developmentErrors);
  }

  // production error handler
  app.use(errorHandlers.productionErrors);

  return app;
}

module.exports.initRoutes = initRoutes;
