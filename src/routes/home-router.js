const express = require("express");
const homeController = require("../controllers/home-controller");
const { catchErrors } = require("../helpers/error-handlers");

const router = express.Router();

function routes(mongoDB) {
  const { getAllData } = homeController(mongoDB);

  router.route("/").get(catchErrors(getAllData));

  // Another home related route here...

  return router;
}

module.exports = routes;
