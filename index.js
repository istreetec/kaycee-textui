/* eslint-disable no-unused-expressions */
const debug = require('debug')('azurenodeapp:index');
const connection = require('./src/db');
const { initRoutes } = require('./src/routes');

const app = require('./src/app');

const port = process.env.PORT || 5001;
const { enabled } = debug;

connection()
  .then(async dbs => {
    // Inject App and mongoDB into Routes.
    const main = await initRoutes(app, dbs.mongoDB);

    main.listen(port, () => {
      const connected = `Ghost closing on port ${port}.`;
      enabled ? debug(connected) : console.log(connected);
    });
  })
  .catch(error => {
    debug(error);
    process.exit(1);
  });
